import React from "react";
import ReactDOM from "react-dom";
import { useRoutes, useRedirect } from "hookrouter";
import { ApolloProvider } from "@apollo/react-hooks";

import { client } from "./apolloClient";
import { DocumentPage } from "./pages/Document";
import { NotFoundPage } from "./pages/NotFound";
import { GlobalStyles } from "./globalStyles";

const DEFAULT_DOCUMENT_ID = "Y8wDM";

interface QueryParams {
  [key: string]: any;
}

const routes = {
  "/d/:documentId": ({ documentId }: QueryParams) => (
    <DocumentPage shortId={documentId} />
  ),
  "/d/:documentId/:artboardIndex": ({
    documentId,
    artboardIndex
  }: QueryParams) => (
    <DocumentPage
      shortId={documentId}
      artboardIndex={artboardIndex ? parseInt(artboardIndex) : undefined}
    />
  )
};

const App: React.FC = () => {
  useRedirect("/", `/d/${DEFAULT_DOCUMENT_ID}`);
  const routeResult = useRoutes(routes);

  return (
    <ApolloProvider client={client}>
      <GlobalStyles />
      {routeResult ?? <NotFoundPage />}
    </ApolloProvider>
  );
};

ReactDOM.render(<App />, document.getElementById("App"));
