import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import styled from "styled-components";
import { A } from "hookrouter";

import { Artboard } from "./Artboard";
import { DocumentView } from "./DocumentView";
import { DocumentTopbar, ArtboardPreview } from "../components";

interface DocumentQueryData {
  share: ApiTypes.Share;
}

interface DocumentQueryVariables {
  shortId: string;
}

export const DOCUMENT_QUERY = gql`
  query DocumentQuery($shortId: String!) {
    share(shortId: $shortId) {
      shortId
      version {
        document {
          name
          artboards {
            entries {
              name
              isArtboard
              files {
                url
                height
                width
                scale
                thumbnails {
                  url
                  height
                  width
                }
              }
            }
          }
        }
      }
    }
  }
`;

interface IProps {
  shortId: string;
  artboardIndex?: number;
}

const PreviewGrid = styled.div`
  padding: 25px;
  display: grid;
  grid-template-columns: repeat(5, minmax(0, 1fr));
  grid-gap: 50px;
`;

const StyledLink = styled(A)`
  text-decoration: none;
`;

export const DocumentPage: React.FC<IProps> = ({ shortId, artboardIndex }) => {
  const { loading, error, data } = useQuery<
    DocumentQueryData,
    DocumentQueryVariables
  >(DOCUMENT_QUERY, {
    variables: { shortId }
  });

  if (loading) {
    return <p>Loading...</p>;
  }

  if (error ?? !data) {
    return <p>Error :(</p>;
  }

  const { name, artboards } = data.share.version.document;

  if (artboardIndex !== undefined) {
    const artboard = artboards.entries[artboardIndex - 1];
    return (
      <Artboard
        artboard={artboard}
        artboardIndex={artboardIndex}
        artboardMax={artboards.entries.length}
        documentId={shortId}
      />
    );
  }

  return (
    <DocumentView name={name} artboards={artboards.entries} shortId={shortId} />
  );
};
