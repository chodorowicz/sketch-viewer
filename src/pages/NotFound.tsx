import React from "react";

export const NotFoundPage: React.FC = () => {
  return (
    <section>
      <h1>Page not found.</h1>
      <a href="/">Go back to home page</a>
    </section>
  );
};
