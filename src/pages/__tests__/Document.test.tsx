import React from "react";
import { render, wait } from "@testing-library/react";
import { MockedProvider } from "@apollo/react-testing";

import { DocumentPage, DOCUMENT_QUERY } from "../Document";

const result = {
  data: {
    share: {
      shortId: "someId",
      version: {
        document: {
          name: "screens",
          artboards: {
            entries: [
              {
                name: "Xerox Artboard",
                isArtboard: true,
                files: [
                  {
                    height: 100,
                    width: 100,
                    scale: 2,
                    url: "some-url",
                    thumbnails: [{ url: "url", height: 100, width: 100 }]
                  }
                ]
              }
            ]
          }
        }
      }
    }
  }
};

const mocks = [
  {
    request: {
      query: DOCUMENT_QUERY,
      variables: {
        shortId: "someId"
      }
    },
    result
  }
];

describe("Document", () => {
  it("should display loading info, artboard name and correct number of artboards", async () => {
    const { getByText, getAllByTestId } = render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <DocumentPage shortId="someId" />
      </MockedProvider>
    );
    expect(getByText("Loading...")).toBeDefined();
    await wait(() => expect(getByText("Xerox Artboard")).toBeDefined());
    expect(getAllByTestId("ArtboardPreview")).toHaveLength(1);
  });
});
