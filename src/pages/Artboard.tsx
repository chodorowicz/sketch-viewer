import React from "react";
import { navigate } from "hookrouter";
import styled from "styled-components";

import { ArtboardTopbar } from "../components";

interface IProps {
  artboard: ApiTypes.Artboard;
  artboardIndex: number;
  artboardMax: number;
  documentId: string;
}

const ImageContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: calc(100vh - 60px);
  padding: 50px;
  box-sizing: border-box;
`;

const StyledImage = styled.img`
  max-width: 100%;
  max-height: 100%;
`;

export const Artboard: React.FC<IProps> = ({
  artboard,
  artboardIndex,
  artboardMax,
  documentId
}) => {
  const file = artboard.files.find(file => file.scale === 1);
  return (
    <>
      <ArtboardTopbar
        currentPage={artboardIndex}
        lastPage={artboardMax}
        name={artboard.name}
        previousAction={
          artboardIndex > 1
            ? () => navigate(`/d/${documentId}/${artboardIndex - 1}`)
            : undefined
        }
        nextAction={
          artboardIndex < artboardMax
            ? () => navigate(`/d/${documentId}/${artboardIndex + 1}`)
            : undefined
        }
        closeAction={() => navigate(`/d/${documentId}`)}
      />
      <ImageContainer>
        <StyledImage src={file?.url} />
      </ImageContainer>
    </>
  );
};
