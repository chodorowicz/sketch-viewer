import React from "react";
import styled from "styled-components";
import { DocumentTopbar, ArtboardPreview } from "../components";
import { A } from "hookrouter";

const PreviewGrid = styled.div`
  padding: 25px;
  display: grid;
  grid-template-columns: repeat(5, minmax(0, 1fr));
  grid-gap: 50px;
`;

const StyledLink = styled(A)`
  text-decoration: none;
`;

interface Props {
  name: string;
  artboards: ApiTypes.Artboard[];
  shortId: string;
}

export const DocumentView: React.FC<Props> = ({ name, artboards, shortId }) => {
  return (
    <>
      <DocumentTopbar name={name} />
      <PreviewGrid>
        {artboards.map((entry, index) => {
          const url = entry.files[0]?.thumbnails[0]?.url;
          return (
            <StyledLink href={`/d/${shortId}/${index + 1}`} key={name}>
              <ArtboardPreview
                url={url}
                name={entry.name}
                data-testid="ArtboardPreview"
              />
            </StyledLink>
          );
        })}
      </PreviewGrid>
    </>
  );
};
