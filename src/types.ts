declare namespace ApiTypes {
  interface Thumbnail {
    url: string;
    height: number;
    width: number;
  }

  interface File {
    url: string;
    height: number;
    width: number;
    scale: number;
    thumbnails: Thumbnail[];
  }

  interface Artboard {
    name: string;
    isArtboard: true;
    files: File[];
  }

  interface Document {
    name: string;
    artboards: {
      entries: Artboard[];
    };
  }

  interface Share {
    shortId: string;
    version: {
      document: Document;
    };
  }
}

declare module "*.svg" {
  const content: any;
  export default content;
}
