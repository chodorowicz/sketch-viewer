import React from "react";
import styled from "styled-components";

const Container = styled.div`
  background: #ffffff;
  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.1);
`;

interface Props {
  className?: string;
}

export const TopBar: React.FC<Props> = ({ children, className }) => {
  return <Container className={className}>{children}</Container>;
};
