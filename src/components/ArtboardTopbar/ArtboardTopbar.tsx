import React from "react";
import styled from "styled-components";

import Close from "./close.svg";
import Previous from "./arrow-left.svg";
import Next from "./arrow-right.svg";
import Separator from "../../assets/separator.svg";
import { TopBar } from "../TopBar/TopBar";

const StyledTopbar = styled(TopBar)`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 1fr;
`;

const SideContainer = styled.div`
  grid-area: 1 / 1 / 1 / 1;
  display: flex;
  align-items: center;
`;

const NavigationContainer = styled.nav`
  padding: 15px;
  display: flex;
  justify-items: space-between;
  align-items: center;
`;

const Pages = styled.span`
  padding-left: 15px;
  padding-right: 15px;
  color: rgba(0, 0, 0, 0.4);
  span {
    padding-left: 5px;
    padding-right: 5px;
  }
`;

const Title = styled.h1`
  padding: 15px;
  font-size: 16px;
  margin: 0;
  font-weight: normal;
  color: #000000;
  grid-area: 1 / 1 / 1 / 1;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledClose = styled(Close)`
  cursor: pointer;
  padding: 15px;
`;

const StyledPrevious = styled(Previous)<{ isActive: boolean }>`
  cursor: ${props => (props.isActive ? "pointer" : "default")};
  opacity: ${props => (props.isActive ? 1 : 0.2)};
`;

const StyledNext = styled(Next)<{ isActive: boolean }>`
  cursor: ${props => (props.isActive ? "pointer" : "default")};
  opacity: ${props => (props.isActive ? 1 : 0.2)};
`;

interface IProps {
  name: string;
  currentPage: number;
  lastPage: number;
  closeAction?: () => void;
  previousAction?: () => void;
  nextAction?: () => void;
}

export const ArtboardTopbar: React.FC<IProps> = ({
  name,
  currentPage,
  lastPage,
  closeAction,
  nextAction,
  previousAction
}) => {
  return (
    <StyledTopbar>
      <SideContainer>
        <StyledClose onClick={closeAction} />
        <Separator />
        <NavigationContainer>
          <StyledPrevious
            onClick={previousAction}
            isActive={previousAction !== undefined}
          />
          <Pages>
            {currentPage}
            <span>/</span>
            {lastPage}
          </Pages>
          <StyledNext
            onClick={nextAction}
            isActive={nextAction !== undefined}
          />
        </NavigationContainer>
      </SideContainer>
      <Title>{name}</Title>
    </StyledTopbar>
  );
};
