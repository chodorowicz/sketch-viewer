import React from "react";
import styled from "styled-components";

const Container = styled.div`
  display: grid;
  grid-template-rows: 1fr auto;
  height: 100%;
`;

const ImageContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Title = styled.h2`
  font-size: 14px;
  margin-top: 15px;
  color: rgba(0, 0, 0, 0.65);
  font-weight: lighter;
  text-align: center;
`;

const PreviewImage = styled.img`
  max-width: 100%;
`;

interface IProps {
  url: string;
  name: string;
}

export const ArtboardPreview: React.FC<IProps> = ({
  url,
  name,
  ...otherProps
}) => {
  return (
    <Container {...otherProps}>
      <ImageContainer>
        <PreviewImage src={url} />
      </ImageContainer>
      <Title>{name}</Title>
    </Container>
  );
};
