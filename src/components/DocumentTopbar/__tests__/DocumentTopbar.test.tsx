import React from "react";
import { render } from "@testing-library/react";

import { DocumentTopbar } from "../DocumentTopbar";

describe("DocumentTopbar", () => {
  it("should display document title", async () => {
    const { getByText } = render(<DocumentTopbar name="hello" />);
    expect(getByText("hello")).toBeDefined();
  });
});
