import React from "react";
import styled from "styled-components";

import Separator from "../../assets/separator.svg";
import SketchLogo from "./sketch-logo.svg";
import { TopBar } from "../TopBar/TopBar";

const Title = styled.h1`
  padding: 15px;
  font-size: 16px;
  margin: 0;
  font-weight: normal;
  color: #000000;
`;

const StyledLogo = styled(SketchLogo)`
  padding: 15px;
`;

const StyledTopbar = styled(TopBar)`
  display: flex;
  align-items: center;
`;

interface IProps {
  name: string;
}

export const DocumentTopbar: React.FC<IProps> = ({ name }) => {
  return (
    <StyledTopbar>
      <StyledLogo />
      <Separator />
      <Title>{name}</Title>
    </StyledTopbar>
  );
};
